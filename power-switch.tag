<power-switch>
  <input type="checkbox" id="switch" onclick={toggle} checked={this.parent.systemIsOn}/>
  <label for="switch">Toggle</label>

  toggle (event) {
    turnOn = event.target.checked
    if (turnOn) {
      this.turnOnDefault()
    } else {
      this.turnOff()
    }
  }

  turnOnDefault () {
    $.updateNightLight('noise', {sounds: {
      distant_waterfall: 1,
      rain_drops: 1
    }})
    $.updateNightLight('light', {color: [255, 147, 0, 0]})
    $('input[type=color]').val('#ff9300')
    $('[data-id="distant_waterfall"]').css('background-color', `rgba(0, 0, 50, 0.5)`)
    $('[data-id="rain_drops"]').css('background-color', `rgba(0, 0, 50, 0.5)`)
  }

  turnOff () {
    $.updateNightLight('noise', {sounds: {}})
    $.updateNightLight('light', {color: [0,0,0,0]})
    $('input[type=color]').val('#000000')
    sounds_list = this.parent.tags.sounds.sounds_list
    for (var i = 0; i < sounds_list.length; i++) {
      sound_id = sounds_list[i]
      $(`[data-id="${sound_id}"]`).css('background-color', `rgba(0, 0, 50, 0)`)
    }
  }

  <style>
  input[type=checkbox]{
    height: 0;
    width: 0;
    visibility: hidden;
  }

  label {
    cursor: pointer;
    text-indent: -9999px;
    width: 80px;
    height: 40px;
    background: grey;
    display: block;
    border-radius: 100px;
    position: relative;
  }

  label:after {
    content: '';
    position: absolute;
    top: 5px;
    left: 5px;
    width: 30px;
    height: 30px;
    background: #fff;
    border-radius: 30px;
    transition: 0.3s;
  }

  input:checked + label {
    background: #bada55;
  }

  input:checked + label:after {
    left: calc(100% - 5px);
    transform: translateX(-100%);
  }

  label:active:after {
    width: 35px;
  }
  </style>
</power-switch>
