<color>
  <h3>Change the color</h3>
  <input name="Color Picker" type="color" oninput={updateColor} />

  this.hexToRgb = (hex) => {
    result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return [
      parseInt(result[1], 16),
      parseInt(result[2], 16),
      parseInt(result[3], 16)
    ]
  }

  this.intToHex = (int) => {
    hex = Number(int).toString(16)
    if (hex.length < 2) {
      hex = "0" + hex;
    }
    return hex;
  }

  this.rgbToHex = (r,g,b) => {
    red = this.intToHex(r);
    green = this.intToHex(g);
    blue = this.intToHex(b);
    return '#'+red+green+blue;
  };

  this.updateColor = $.debounce(750, (e) => {
    hexColor = e.target.value
    rgb = this.hexToRgb(hexColor)
    white = Math.floor((rgb[0] + rgb[1] + rgb[2]) / 3)
    rgbw = [...rgb, 0]
    data = {color: rgbw}
    $.updateNightLight('light', data)
    // if rgb is black, let app know the light is off
    this.parent.lightIsOn = (rgb[0] || rgb[1] || rgb[2] ) ? true : false
    this.parent.update()
  })

  <!-- Set Initial Color -->
  $.getNightLight('light')
  .done( (data, textStatus, jqXHR ) => {
    rgb = data.color.slice(0,3)
    hex = this.rgbToHex(...rgb)
    $('color input').val(hex)
    $('color').css('display', 'flex')
  })
  .fail( (jqXHR, textStatus, errorThrown) => {
    console.log(jqXHR.responseText)
  })

  <style>
    :scope {
      display: none;
      font-size: 2rem;
      flex-direction: column;
      align-items: center;
    }
    h3 { color: #444 }
    input[type="color"] {
      width: 150px;
      height: 150px;
    }
  </style>
</color>
