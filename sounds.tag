<sounds>
  <h3>Change the sounds</h3>
  <div id="sounds-wrapper">
    <div class="sound" each={ sound in this.sounds_list } data-id={sound} data-val={0} onclick={updateSounds}>
      { this.snakeToTitle(sound) }
    </div>
  </div>

  <!-- Set Initial Sounds -->
  this.sounds = {}
  $.getNightLight('noise')
  .done( (data, textStatus, jqXHR ) => {
    sounds = data.sounds
    for ([id, val] of Object.entries(sounds)) {
      elem = $(`.sound[data-id=${id}]`)
      elem.css('background-color', `rgba(0, 0, 50, ${val/2})`)
      elem.data('val', val)
    }
  })
  .fail( (jqXHR, textStatus, errorThrown) => {
    console.log(jqXHR.responseText)
  })

  updateSounds (e) {
    elem = $(e.target)
    data = elem.data()

    <!-- toggle data value between 0 and 1 -->
    data.val = data.val === 1 ?  0 : 1
    elem.data("val", data.val)

    <!-- update sound object -->
    if (data.val > 0) {
      this.sounds[data.id] = data.val
    } else {
      delete this.sounds[data.id]
    }

    <!-- post it up to the thing -->
    $.updateNightLight('noise', {sounds: this.sounds})

    <!-- update background color -->
    elem.css('background-color', `rgba(0, 0, 50, ${data.val/2})`)

    // let parent know if sounds are off
    this.parent.soundIsOn = !$.isEmptyObject(this.sounds)
    this.parent.update()
  }

  this.sounds_list = [
      "birds",
      "brook",
      "brown_noise",
      "close_waterfall",
      "coastal_wind",
      "creek",
      "distant_thunder",
      "distant_waterfall",
      "fan",
      "fire",
      "forest_wind",
      "pink_noise",
      "pouring_rain",
      "rain_drops",
      "stream",
      "summer_night",
      "white_noise",
  ]


  this.snakeToTitle = (str) => {
    upper = str.charAt(0).toUpperCase() + str.substr(1)
    return upper.replace(
        /(\_\w)/g,
        (matches) => " " + matches[1].toUpperCase()
    );
  }

  <style>
    :scope {
      font-size: 2rem;
      display: flex;
      flex-direction: column;
      align-items: center;
    }
    #sounds-wrapper {
      display: flex;
      justify-content: space-between;
      flex-wrap: wrap;
      width: 950px;
    }
    .sound {
      padding: 20px;
      margin: 5px;
      border: 1px solid;
      border-radius: 10px;
      cursor: pointer;
    }
    h3 { color: #444 }
  </style>
</sounds>
