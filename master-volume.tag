<master-volume>
  <h3>Change the master volume</h3>
  <input type="range" min="0" max="1" step=".01" value="0.2" oninput={updateMasterVolume}>

  <!-- Set Initial Color -->
  $.getNightLight('noise')
  .done( (data, textStatus, jqXHR ) => {
    masterVolume = data.master_volume
    $('master-volume input').val(masterVolume)
  })
  .fail( (jqXHR, textStatus, errorThrown) => {
    console.log(jqXHR.responseText)
  })

  this.updateMasterVolume = $.debounce(750, (e) => {
    masterVolume = e.target.value
    $.updateNightLight('noise', {'master_volume': masterVolume} )
  })

  <style>
    :scope {
      font-size: 2rem;
      display: flex;
      flex-direction: column;
      align-items: center;
    }
    h3 { color: #444 }
  </style>
</master-volume>
