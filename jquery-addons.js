// Some utitlity functions, added to jquery namespace since we're already
// pulling that in...yeah it's kinda dumb
$.debounce = (wait, func) => {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      func.apply(context, args);
    };
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
};

$.getNightLight = (endpoint) => {
  return $.ajax({
    url: `http://192.168.0.11:5000/${endpoint}`,
    type: "GET",
  })
}

$.updateNightLight = (endpoint, data) => {
  $.ajax({
    url: `http://192.168.0.11:5000/${endpoint}`,
    type: "POST",
    data: JSON.stringify(data, null, '\t'),
    contentType: 'application/json;charset=UTF-8',
  })
  .done( (data, textStatus, jqXHR ) => {
    console.log(data);
    console.log(textStatus)
  })
  .fail( (jqXHR, textStatus, errorThrown) => {
    console.log(jqXHR.responseText)
  })
}
