<app>
  <power-switch></power-switch>
  <color></color>
  <brightness></brightness>
  <sounds></sounds>
  <master-volume></master-volume>

  this.lightIsOn = false
  this.soundIsOn = false
  this.systemIsOn = this.lightIsOn || this.soundIsOn

  $.getNightLight('light')
  .done( (data, textStatus, jqXHR ) => {
    rgb = data.color
    this.lightIsOn = (rgb[0] || rgb[1] || rgb[2] ) ? true : false
    this.update()
  })
  .fail( (jqXHR, textStatus, errorThrown) => {
    console.log(jqXHR.responseText)
  })

  $.getNightLight('noise')
  .done( (data, textStatus, jqXHR ) => {
    this.soundIsOn = !$.isEmptyObject(data.sounds)
    this.update()
  })
  .fail( (jqXHR, textStatus, errorThrown) => {
    console.log(jqXHR.responseText)
  })

  this.on('update', () => {
    this.systemIsOn = this.lightIsOn || this.soundIsOn
  })
</app>
