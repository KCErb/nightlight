<brightness>
  <h3>Change the brightness</h3>
  <input type="range" min="0" max="0.1" step=".01" oninput={updateBrightness}>

  this.updateBrightness = $.debounce(750, (e) => {
    brightness = e.target.value
    $.updateNightLight('light', {brightness})
  })

  <!-- Set Initial Brightness -->
  $.getNightLight('light')
  .done( (data, textStatus, jqXHR ) => {
    brightness = data.brightness
    $('brightness input').val(brightness)
    $('brightness').css('display', 'flex')
  })
  .fail( (jqXHR, textStatus, errorThrown) => {
    console.log(jqXHR.responseText)
  })

  <style>
    :scope {
      font-size: 2rem;
      display: none;
      flex-direction: column;
      align-items: center;
    }
    h3 { color: #444 }
  </style>
</brightness>
